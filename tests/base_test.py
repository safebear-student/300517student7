import unittest
from utils import Parameter
from page10_welcome_page import WelcomePage
from page20_login_page import LoginPage
from page30_main_page import MainPage
from page40_frames_page import FramePage

class BaseTest(unittest.TestCase):
    param = Parameter()
    WelcomePage = WelcomePage(param.w,param.rootUrl)
    LoginPage = LoginPage(param.w,param.rootUrl)
    MainPage = MainPage(param.w,param.rootUrl)
    FramePage = FramePage(param.w,param.rootUrl)

    def setUp(self):
        self.param.w.get(self.param.rootUrl)
        self.param.w.maximize_window()
        assert self.WelcomePage.check_page()
    @classmethod
    def tearDownClass(cls):
        cls.param.w.quit()

