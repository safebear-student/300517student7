from base_test import BaseTest
class TestCases (BaseTest):
    def test_01_test_login_page_login(self):
        # Step 1: Clickon Login and the Login Page Loads
       assert self.WelcomePage.click_login(self.LoginPage)
        # Step 2: Login to the webpage and confirm the main page appears
       assert self.LoginPage.login(self.MainPage, "testuser", "testing")
        # Step 3: clicking on the logout link, checking we are on the welcome page
       assert self.MainPage.click_logout(self.WelcomePage)

